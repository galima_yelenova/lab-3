﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace lab3_1_1_
{
    class Program
    {
        static bool IsZip(string s)
        {
            return Regex.IsMatch(s, @"^\d{5}(\-\d{4})?$");
        }
        static bool IsPhone(string s)
        {
            return Regex.IsMatch(s, @"^\(?\d{3}\)?[\s-]?\d{3}-?\d{4}");
        }
        static void Main(string[] args)
        {
            string s = Console.ReadLine();
            if (IsPhone(s))
            {
                Console.Write(s);
                Console.Write(" is a phone number");
            }
            if (IsZip(s))
            {
                Console.Write(s);
                Console.Write(" is a zip code");
            }
            else if (!IsPhone(s) && !IsZip(s))
            {
                Console.Write(s);
                Console.Write(" is unknown");
            }

            Console.ReadKey();
        }
    }
}
